export class datousuario {
    nombre: string;
    cedula: number;
    celular: string;
    correo: string;
    contrasena: string;
    departamento: string;
    ciudad: string;
    barrio: string;
    direccion: string;
    salario: number;
    otrosingresos: number;
    gastosmensuales: number;
    gastosfinanzas: number;
}