import { Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { InsercionbdService } from '@services/insercionbd.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  dataUser = new FormGroup({
    correo: new FormControl(''),
    contrasena: new FormControl('')
  });
  constructor(private _http: InsercionbdService,
              private route: Router) { }

  ngOnInit(): void {
    
  }
  verificacion()
  {
    this._http.loginUsuario(this.dataUser.value).subscribe(
      Response=>{
        console.log(Response);
        this.route.navigateByUrl('/profile');
        localStorage.setItem('auth_token', Response.token);
      },
      err=>{
        console.log(<any>err);
        
      }
    )
  }
}
