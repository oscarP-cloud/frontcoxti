import { datousuario } from './../../models/datousuario.model';
import { InsercionbdService } from '@services/insercionbd.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {
  // definicion de formgroup del formulario
  formulario: FormGroup

  constructor(private _http: InsercionbdService,
              private route: Router,
              private fb: FormBuilder) { }

  ngOnInit(): void {
    this.crearFormulario();
   // this.guardar();
  }
  // Se crea este metodo para tomar los datos que se llenan en el formulario
  // para despues usar el servicio y agregar los datos.
  guardar(){
    console.log(this.formulario.invalid );
    
    if( this.formulario.invalid ){ 
      return console.log('Campos erroneos');
      
     }else
     {
       this._http.registro(this.formulario.value).subscribe( 
         (Response: datousuario) => {
         console.log(Response);
       },
       err => {
         if (err.status == 200) {
          this.route.navigateByUrl('/home');
         }else{
           console.error('Usuario ya existe');
         }
       () => console.log('Ya finalizo la inserción')
        });
      
     }

  }
  // Validacion de datos encontrados en el formulario
  // se establecen longitud, caracteres y tipo.
  crearFormulario() {
  this.formulario = this.fb.group({
      nombre: ['',[Validators.required, Validators.minLength(2)]],
      cedula: ['',[Validators.required, Validators.maxLength(10)]],
      celular: ['',[Validators.required]],
      correo: ['',[Validators.required, Validators.email, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      contrasena: ['',[Validators.required, Validators.minLength(8)]],
      departamento: ['',[Validators.required]],
      ciudad: ['',[Validators.required]],
      barrio: ['',[Validators.required]],
      direccion: ['',[Validators.required]],
      salario: ['',[Validators.required]],
      otrosingresos: ['',[Validators.required]],
      gastosmensuales: ['',[Validators.required]],
      gastosfinanzas: ['',[Validators.required]],
    });
  }
}
