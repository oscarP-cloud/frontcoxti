import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  logout() {
    localStorage.removeItem('auth_token');
  }
 
   logIn(): boolean {
    return (localStorage.getItem('auth_token') !== null);
  }
}
