import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class InsercionbdService {
  private url = 'http://localhost:1200/'
  constructor(private _httpClient: HttpClient) { }

  registro(usuario):Observable<any> {
    let params = JSON.stringify(usuario);
    let header = new HttpHeaders().set('Content-Type','application/json');
    return this._httpClient.post(`${ this.url }api/registro`,params, {headers:header})
  }
  salario(){
    return this._httpClient.get(`${ this.url }api/salario`)
  }
  loginUsuario(datos):Observable<any>{
    let params = JSON.stringify(datos);
    let header = new HttpHeaders().set('Content-Type','application/json');
    return this._httpClient.post(this.url+'api/login',params,{headers:header});
  }
  consulta(datos){
    return this._httpClient.get(this.url+'api/datosUsuario');
  }
  
}
